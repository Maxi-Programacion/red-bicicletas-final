const nodemailer = require('nodemailer');
const sgTransport = require('nodemailer-sendgrid-transport');

let mailConfig;
if (process.env.NODE_ENV === 'production') {
  const options = {
    auth: {
      api_key: process.env.SENDGRID_API_SECRET
    }
  }
  mailConfig = sgTransport(options);
} else {
  if (process.env.NODE_ENV === 'staging') {
    console.log('xxxxxxxxxxxxxxx');
    const options = {
      auth: {
        api_key: process.env.SENDGRID_API_SECRET
      }
    }
    mailConfig = sgTransport(options);
  }
  else {
    mailConfig = {
      host: 'smtp.ethereal.email',
      port: 587,
      auth: {
        user: process.env.ethereal_user,
        pass: process.env.ethereal_pwd 
      }
    };
  }
}

// const mailConfig = {
//   host: 'smtp.ethereal.email',
//   port: 587,
//   auth: {
//     user: 'sammie.trantow34@ethereal.email',
//     pass: 'Q9X1j8Fskw34eYq7MK'
//   }
// };

module.exports = nodemailer.createTransport(mailConfig);